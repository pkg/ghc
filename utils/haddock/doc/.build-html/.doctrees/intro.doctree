���s      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Introduction�h]�h	�Text����Introduction�����}�(hh�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�4/home/zubin/ghcs/hs-main/utils/haddock/doc/intro.rst�hKubh	�	paragraph���)��}�(h��This is Haddock, a tool for automatically generating documentation from
annotated Haskell source code. Haddock was designed with several goals
in mind:�h]�h��This is Haddock, a tool for automatically generating documentation from
annotated Haskell source code. Haddock was designed with several goals
in mind:�����}�(hh1hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(hXc  When documenting APIs, it is desirable to keep the documentation
close to the actual interface or implementation of the API,
preferably in the same file, to reduce the risk that the two become
out of sync. Haddock therefore lets you write the documentation for
an entity (function, type, or class) next to the definition of the
entity in the source code.
�h]�h.)��}�(hXb  When documenting APIs, it is desirable to keep the documentation
close to the actual interface or implementation of the API,
preferably in the same file, to reduce the risk that the two become
out of sync. Haddock therefore lets you write the documentation for
an entity (function, type, or class) next to the definition of the
entity in the source code.�h]�hXb  When documenting APIs, it is desirable to keep the documentation
close to the actual interface or implementation of the API,
preferably in the same file, to reduce the risk that the two become
out of sync. Haddock therefore lets you write the documentation for
an entity (function, type, or class) next to the definition of the
entity in the source code.�����}�(hhJhhHhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhDubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhh?hhhh,hNubhC)��}�(hX�  There is a tremendous amount of useful API documentation that can be
extracted from just the bare source code, including types of exported
functions, definitions of data types and classes, and so on. Haddock
can therefore generate documentation from a set of straight Haskell
98 modules, and the documentation will contain precisely the
interface that is available to a programmer using those modules.
�h]�h.)��}�(hX�  There is a tremendous amount of useful API documentation that can be
extracted from just the bare source code, including types of exported
functions, definitions of data types and classes, and so on. Haddock
can therefore generate documentation from a set of straight Haskell
98 modules, and the documentation will contain precisely the
interface that is available to a programmer using those modules.�h]�hX�  There is a tremendous amount of useful API documentation that can be
extracted from just the bare source code, including types of exported
functions, definitions of data types and classes, and so on. Haddock
can therefore generate documentation from a set of straight Haskell
98 modules, and the documentation will contain precisely the
interface that is available to a programmer using those modules.�����}�(hhbhh`hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh\ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhh?hhhh,hNubhC)��}�(hX�  Documentation annotations in the source code should be easy on the
eye when editing the source code itself, so as not to obscure the
code and to make reading and writing documentation annotations easy.
The easier it is to write documentation, the more likely the
programmer is to do it. Haddock therefore uses lightweight markup in
its annotations, taking several ideas from
`IDoc <http://freshmeat.sourceforge.net/projects/idoc/>`__. In fact,
Haddock can understand IDoc-annotated source code.
�h]�h.)��}�(hX�  Documentation annotations in the source code should be easy on the
eye when editing the source code itself, so as not to obscure the
code and to make reading and writing documentation annotations easy.
The easier it is to write documentation, the more likely the
programmer is to do it. Haddock therefore uses lightweight markup in
its annotations, taking several ideas from
`IDoc <http://freshmeat.sourceforge.net/projects/idoc/>`__. In fact,
Haddock can understand IDoc-annotated source code.�h]�(hXw  Documentation annotations in the source code should be easy on the
eye when editing the source code itself, so as not to obscure the
code and to make reading and writing documentation annotations easy.
The easier it is to write documentation, the more likely the
programmer is to do it. Haddock therefore uses lightweight markup in
its annotations, taking several ideas from
�����}�(hXw  Documentation annotations in the source code should be easy on the
eye when editing the source code itself, so as not to obscure the
code and to make reading and writing documentation annotations easy.
The easier it is to write documentation, the more likely the
programmer is to do it. Haddock therefore uses lightweight markup in
its annotations, taking several ideas from
�hhxhhhNhNubh	�	reference���)��}�(h�:`IDoc <http://freshmeat.sourceforge.net/projects/idoc/>`__�h]�h�IDoc�����}�(h�IDoc�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�h��refuri��/http://freshmeat.sourceforge.net/projects/idoc/�uh+h�hhxubh�=. In fact,
Haddock can understand IDoc-annotated source code.�����}�(h�=. In fact,
Haddock can understand IDoc-annotated source code.�hhxhhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhtubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhh?hhhh,hNubhC)��}�(hX|  The documentation should not expose any of the structure of the
implementation, or to put it another way, the implementer of the API
should be free to structure the implementation however he or she
wishes, without exposing any of that structure to the consumer. In
practical terms, this means that while an API may internally consist
of several Haskell modules, we often only want to expose a single
module to the user of the interface, where this single module just
re-exports the relevant parts of the implementation modules.

Haddock therefore understands the Haskell module system and can
generate documentation which hides not only non-exported entities
from the interface, but also the internal module structure of the
interface. A documentation annotation can still be placed next to the
implementation, and it will be propagated to the external module in
the generated documentation.
�h]�(h.)��}�(hX  The documentation should not expose any of the structure of the
implementation, or to put it another way, the implementer of the API
should be free to structure the implementation however he or she
wishes, without exposing any of that structure to the consumer. In
practical terms, this means that while an API may internally consist
of several Haskell modules, we often only want to expose a single
module to the user of the interface, where this single module just
re-exports the relevant parts of the implementation modules.�h]�hX  The documentation should not expose any of the structure of the
implementation, or to put it another way, the implementer of the API
should be free to structure the implementation however he or she
wishes, without exposing any of that structure to the consumer. In
practical terms, this means that while an API may internally consist
of several Haskell modules, we often only want to expose a single
module to the user of the interface, where this single module just
re-exports the relevant parts of the implementation modules.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubh.)��}�(hXj  Haddock therefore understands the Haskell module system and can
generate documentation which hides not only non-exported entities
from the interface, but also the internal module structure of the
interface. A documentation annotation can still be placed next to the
implementation, and it will be propagated to the external module in
the generated documentation.�h]�hXj  Haddock therefore understands the Haskell module system and can
generate documentation which hides not only non-exported entities
from the interface, but also the internal module structure of the
interface. A documentation annotation can still be placed next to the
implementation, and it will be propagated to the external module in
the generated documentation.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK(hh�ubeh}�(h!]�h#]�h%]�h']�h)]�uh+hBhh?hhhh,hNubhC)��}�(hXh  Being able to move around the documentation by following hyperlinks
is essential. Documentation generated by Haddock is therefore
littered with hyperlinks: every type and class name is a link to the
corresponding definition, and user-written documentation annotations
can contain identifiers which are linked automatically when the
documentation is generated.
�h]�h.)��}�(hXg  Being able to move around the documentation by following hyperlinks
is essential. Documentation generated by Haddock is therefore
littered with hyperlinks: every type and class name is a link to the
corresponding definition, and user-written documentation annotations
can contain identifiers which are linked automatically when the
documentation is generated.�h]�hXg  Being able to move around the documentation by following hyperlinks
is essential. Documentation generated by Haddock is therefore
littered with hyperlinks: every type and class name is a link to the
corresponding definition, and user-written documentation annotations
can contain identifiers which are linked automatically when the
documentation is generated.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK/hh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhh?hhhh,hNubhC)��}�(h��We might want documentation in multiple formats - online and printed,
for example. Haddock comes with HTML, LaTeX, and Hoogle backends, and
it is structured in such a way that adding new backends is
straightforward.
�h]�h.)��}�(h��We might want documentation in multiple formats - online and printed,
for example. Haddock comes with HTML, LaTeX, and Hoogle backends, and
it is structured in such a way that adding new backends is
straightforward.�h]�h��We might want documentation in multiple formats - online and printed,
for example. Haddock comes with HTML, LaTeX, and Hoogle backends, and
it is structured in such a way that adding new backends is
straightforward.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK6hh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhh?hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]��bullet��-�uh+h=hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�Obtaining Haddock�h]�h�Obtaining Haddock�����}�(hj	  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj  hhhh,hK<ubh.)��}�(h�sDistributions (source & binary) of Haddock can be obtained from its `web
site <http://www.haskell.org/haddock/>`__.�h]�(h�DDistributions (source & binary) of Haddock can be obtained from its �����}�(h�DDistributions (source & binary) of Haddock can be obtained from its �hj  hhhNhNubh�)��}�(h�.`web
site <http://www.haskell.org/haddock/>`__�h]�h�web
site�����}�(h�web
site�hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name��web site�h��http://www.haskell.org/haddock/�uh+h�hj  ubh�.�����}�(h�.�hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK>hj  hhubh.)��}�(h��Up-to-date sources can also be obtained from our public GitHub
repository. The Haddock sources are at
``https://github.com/haskell/haddock``.�h]�(h�fUp-to-date sources can also be obtained from our public GitHub
repository. The Haddock sources are at
�����}�(h�fUp-to-date sources can also be obtained from our public GitHub
repository. The Haddock sources are at
�hj;  hhhNhNubh	�literal���)��}�(h�&``https://github.com/haskell/haddock``�h]�h�"https://github.com/haskell/haddock�����}�(hhhjF  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jD  hj;  ubh�.�����}�(hj4  hj;  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKAhj  hhubeh}�(h!]��obtaining-haddock�ah#]�h%]��obtaining haddock�ah']�h)]�uh+h
hhhhhh,hK<ubh)��}�(hhh]�(h)��}�(h�License�h]�h�License�����}�(hjk  hji  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjf  hhhh,hKFubh.)��}�(h�oThe following license covers this documentation, and the Haddock source
code, except where otherwise indicated.�h]�h�oThe following license covers this documentation, and the Haddock source
code, except where otherwise indicated.�����}�(hjy  hjw  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKHhjf  hhubh	�block_quote���)��}�(hhh]�(h.)��}�(h�7Copyright 2002-2010, Simon Marlow. All rights reserved.�h]�h�7Copyright 2002-2010, Simon Marlow. All rights reserved.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKKhj�  ubh.)��}�(h��Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:�h]�h��Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKMhj�  ubh>)��}�(hhh]�(hC)��}�(h�}Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
�h]�h.)��}�(h�|Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.�h]�h�|Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKQhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj�  ubhC)��}�(h��Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in
the documentation and/or other materials provided with the
distribution.
�h]�h.)��}�(h��Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in
the documentation and/or other materials provided with the
distribution.�h]�h��Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in
the documentation and/or other materials provided with the
distribution.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKThj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj�  ubeh}�(h!]�h#]�h%]�h']�h)]�j  j  uh+h=hh,hKQhj�  ubh.)��}�(hX�  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.�h]�hX�  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS “AS IS” AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKYhj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+j�  hjf  hhhh,hNubeh}�(h!]��license�ah#]�h%]��license�ah']�h)]�uh+h
hhhhhh,hKFubh)��}�(hhh]�(h)��}�(h�Contributors�h]�h�Contributors�����}�(hj   hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKfubh.)��}�(hX,  Haddock was originally written by Simon Marlow. Since it is an open
source project, many people have contributed to its development over the
years. Below is a list of contributors in alphabetical order that we
hope is somewhat complete. If you think you are missing from this list,
please contact us.�h]�hX,  Haddock was originally written by Simon Marlow. Since it is an open
source project, many people have contributed to its development over the
years. Below is a list of contributors in alphabetical order that we
hope is somewhat complete. If you think you are missing from this list,
please contact us.�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhj�  hhubh>)��}�(hhh]�(hC)��}�(h�Ashley Yakeley�h]�h.)��}�(hj  h]�h�Ashley Yakeley�����}�(hj  hj!  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKnhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Benjamin Franksen�h]�h.)��}�(hj6  h]�h�Benjamin Franksen�����}�(hj6  hj8  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKohj4  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Brett Letner�h]�h.)��}�(hjM  h]�h�Brett Letner�����}�(hjM  hjO  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKphjK  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Clemens Fruhwirth�h]�h.)��}�(hjd  h]�h�Clemens Fruhwirth�����}�(hjd  hjf  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKqhjb  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Conal Elliott�h]�h.)��}�(hj{  h]�h�Conal Elliott�����}�(hj{  hj}  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKrhjy  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�David Waern�h]�h.)��}�(hj�  h]�h�David Waern�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKshj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Duncan Coutts�h]�h.)��}�(hj�  h]�h�Duncan Coutts�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKthj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�George Pollard�h]�h.)��}�(hj�  h]�h�George Pollard�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKuhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�George Russel�h]�h.)��}�(hj�  h]�h�George Russel�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKvhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�	Hal Daume�h]�h.)��}�(hj�  h]�h�	Hal Daume�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKwhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�
Ian Lynagh�h]�h.)��}�(hj  h]�h�
Ian Lynagh�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKxhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Isaac Dupree�h]�h.)��}�(hj  h]�h�Isaac Dupree�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKyhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Joachim Breitner�h]�h.)��}�(hj3  h]�h�Joachim Breitner�����}�(hj3  hj5  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKzhj1  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Krasimir Angelov�h]�h.)��}�(hjJ  h]�h�Krasimir Angelov�����}�(hjJ  hjL  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK{hjH  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Lennart Augustsson�h]�h.)��}�(hja  h]�h�Lennart Augustsson�����}�(hja  hjc  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK|hj_  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�
Luke Plant�h]�h.)��}�(hjx  h]�h�
Luke Plant�����}�(hjx  hjz  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK}hjv  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Malcolm Wallace�h]�h.)��}�(hj�  h]�h�Malcolm Wallace�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK~hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Manuel Chakravarty�h]�h.)��}�(hj�  h]�h�Manuel Chakravarty�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Mark Lentczner�h]�h.)��}�(hj�  h]�h�Mark Lentczner�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Mark Shields�h]�h.)��}�(hj�  h]�h�Mark Shields�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Mateusz Kowalczyk�h]�h.)��}�(hj�  h]�h�Mateusz Kowalczyk�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Mike Thomas�h]�h.)��}�(hj  h]�h�Mike Thomas�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj   ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Neil Mitchell�h]�h.)��}�(hj  h]�h�Neil Mitchell�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Oliver Brown�h]�h.)��}�(hj0  h]�h�Oliver Brown�����}�(hj0  hj2  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj.  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Roman Cheplyaka�h]�h.)��}�(hjG  h]�h�Roman Cheplyaka�����}�(hjG  hjI  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjE  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Ross Paterson�h]�h.)��}�(hj^  h]�h�Ross Paterson�����}�(hj^  hj`  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj\  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Sigbjorn Finne�h]�h.)��}�(hju  h]�h�Sigbjorn Finne�����}�(hju  hjw  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjs  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Simon Hengel�h]�h.)��}�(hj�  h]�h�Simon Hengel�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Simon Marlow�h]�h.)��}�(hj�  h]�h�Simon Marlow�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Simon Peyton-Jones�h]�h.)��}�(hj�  h]�h�Simon Peyton-Jones�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Stefan O'Rear�h]�h.)��}�(hj�  h]�h�Stefan O’Rear�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�
Sven Panne�h]�h.)��}�(hj�  h]�h�
Sven Panne�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Thomas Schilling�h]�h.)��}�(hj�  h]�h�Thomas Schilling�����}�(hj�  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Wolfgang Jeltsch�h]�h.)��}�(hj  h]�h�Wolfgang Jeltsch�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubhC)��}�(h�Yitzchak Gale
�h]�h.)��}�(h�Yitzchak Gale�h]�h�Yitzchak Gale�����}�(hj1  hj/  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj+  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhj  hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]�j  j  uh+h=hh,hKnhj�  hhubeh}�(h!]��contributors�ah#]�h%]��contributors�ah']�h)]�uh+h
hhhhhh,hKfubh)��}�(hhh]�(h)��}�(h�Acknowledgements�h]�h�Acknowledgements�����}�(hjV  hjT  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjQ  hhhh,hK�ubh.)��}�(h�QSeveral documentation systems provided the inspiration for Haddock, most
notably:�h]�h�QSeveral documentation systems provided the inspiration for Haddock, most
notably:�����}�(hjd  hjb  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjQ  hhubh>)��}�(hhh]�(hC)��}�(h�;`IDoc <http://freshmeat.sourceforge.net/projects/idoc/>`__
�h]�h.)��}�(h�:`IDoc <http://freshmeat.sourceforge.net/projects/idoc/>`__�h]�h�)��}�(hjy  h]�h�IDoc�����}�(h�IDoc�hj{  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�j�  h��/http://freshmeat.sourceforge.net/projects/idoc/�uh+h�hjw  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjs  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhjp  hhhh,hNubhC)��}�(h�c`HDoc <https://web.archive.org/web/20010603070527/http://www.fmi.uni-passau.de/~groessli/hdoc/>`__
�h]�h.)��}�(h�b`HDoc <https://web.archive.org/web/20010603070527/http://www.fmi.uni-passau.de/~groessli/hdoc/>`__�h]�h�)��}�(hj�  h]�h�HDoc�����}�(h�HDoc�hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�j�  h��Whttps://web.archive.org/web/20010603070527/http://www.fmi.uni-passau.de/~groessli/hdoc/�uh+h�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhjp  hhhh,hNubhC)��}�(h�0`Doxygen <https://www.doxygen.nl/index.html>`__
�h]�h.)��}�(h�/`Doxygen <https://www.doxygen.nl/index.html>`__�h]�h�)��}�(hj�  h]�h�Doxygen�����}�(h�Doxygen�hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�j�  h��!https://www.doxygen.nl/index.html�uh+h�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hBhjp  hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]�j  j  uh+h=hh,hK�hjQ  hhubh.)��}�(h�+and probably several others I've forgotten.�h]�h�-and probably several others I’ve forgotten.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjQ  hhubh.)��}�(h��Thanks to the the members of haskelldoc@haskell.org,
haddock@projects.haskell.org and everyone who contributed to the many
libraries that Haddock makes use of.�h]�(h�Thanks to the the members of �����}�(h�Thanks to the the members of �hj�  hhhNhNubh�)��}�(h�haskelldoc@haskell.org�h]�h�haskelldoc@haskell.org�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��refuri��mailto:haskelldoc@haskell.org�uh+h�hj�  ubh�,
�����}�(h�,
�hj�  hhhNhNubh�)��}�(h�haddock@projects.haskell.org�h]�h�haddock@projects.haskell.org�����}�(hhhj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��refuri��#mailto:haddock@projects.haskell.org�uh+h�hj�  ubh�N and everyone who contributed to the many
libraries that Haddock makes use of.�����}�(h�N and everyone who contributed to the many
libraries that Haddock makes use of.�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjQ  hhubeh}�(h!]��acknowledgements�ah#]�h%]��acknowledgements�ah']�h)]�uh+h
hhhhhh,hK�ubeh}�(h!]��introduction�ah#]�h%]��introduction�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j_  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_images���embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j9  j6  jc  j`  j�  j�  jN  jK  j1  j.  u�	nametypes�}�(j9  Njc  Nj�  NjN  Nj1  Nuh!}�(j6  hj`  j  j�  jf  jK  j�  j.  jQ  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.